/*
    Simple Pomodoro Clock In C
    Copyright (C) 2020  Arthur Bacci

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <ncurses.h>

unsigned int work_seconds;
unsigned int pause_seconds;

void draw_progress_bar(unsigned int _step, unsigned int max)
{
    unsigned int step;
    
    step = COLS * _step / max;
    move(1, 0);
    for (int i = 0; i < step; i++)
    {
        addch('#');
    }
}

int main(int argc, char **argv)
{
    int ch;
    unsigned char continue_loop;
    unsigned char playing;
    unsigned char _continue;
    
    initscr();
    noecho();
    
    work_seconds = 1500; // 60 * 25
    pause_seconds = 300; // 60 * 5
    
    if (argc > 1)
    {	
	sscanf(argv[1], "%u", &work_seconds);
    }
    if (argc > 2)
    {
	sscanf(argv[2], "%u", &pause_seconds);
    }
    
    mvprintw(0, 0, "Press m for help\n");

    continue_loop = 1;
    while (continue_loop)
    {
	ch = getch();
	clear();
	switch (ch)
	{
	case 'm':
	    mvprintw(0, 0, "Press s to start\n");
	    printw("Press p to pause\n");
	    printw("Press r to reset\n");
	    printw("Press q to quit\n");
	    break;
	case 's':
	    nodelay(stdscr, TRUE);
	    playing = 1;
	    _continue = 0;
	    while (playing)
	    {
		// WORK //
		for (unsigned int i = 0; i < 100; i++)
		{
		    clear();
		    mvprintw(0, 0, "WORK");
		    draw_progress_bar(i, 100);
		    refresh();
		    ch = getch();
		    if (ch != ERR)
		    {
			if (ch == 'p')
			{
			    while (getch() == ERR)
			    {
			    }
			}
			else if (ch == 'q')
			{
			    playing = 0;
			    continue_loop = 0;
			    break;
			}
			else if (ch == 'r')
			{
			    _continue = 1;
			    break;
			}
		    }
		    napms(1000 * work_seconds / 100);
		}
		if (!playing)
		{
		    break;
		}
		if (_continue)
		{
		    _continue = 0;
		    continue;
		}
		// PAUSE //
		for (unsigned int i = 0; i < 100; i++)
		{
		    clear();
		    mvprintw(0, 0, "PAUSE");
		    draw_progress_bar(i, 100);
		    refresh();
		    ch = getch();
		    if (ch != ERR)
		    {
			if (ch == 'p')
			{
			    while (getch() == ERR)
			    {
			    }
			}
			else if (ch == 'q')
			{
			    playing = 0;
			    continue_loop = 0;
			    break;
			}
			else if (ch == 'r')
			{
			    _continue = 1;
			    break;
			}
		    }
		    napms(1000 * pause_seconds / 100);
		}
		if (_continue)
		{
		    _continue = 0;
		    continue;
		}
	    }
	    nodelay(stdscr, FALSE);
	    break;
	case 'q':
	    continue_loop = 0;
	    break;
	}
	refresh();
    }
    
    endwin();
    return 0;
}

